FROM centos:centos7

RUN sed -i "s/tsflags=nodocs/# tsflags=nodocs/" /etc/yum.conf

RUN yum install -y man man-pages man-db

RUN rpm -qa | xargs yum -y reinstall
